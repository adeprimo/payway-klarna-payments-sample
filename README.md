# Tulo Payway - Klarna Payments Sample

Sample application that shows how to embed Klarna Payments through Payway to enable customers to purchase directly on the newspaper site. 
This sample features a use case where you acquire an access token with an identity using the password grant.
The access token is then used to complete the purchase and check access using the Payway API.

This is a ruby application which requires that you have ruby interpreter along with "bundler" installed on your system.

### This is by no means a complete tutorial on how you integrate Payway - Klarna payments checkout into your site. It is only meant to be a guide.

### How do I get set up? ###

Create file settings/config.yml if not exits, with contents, and fill in the blanks. Contact Adeprimo if you have any questions.

```
client_id: 
client_secret: 
scopes: /external/klarna_payments/r /external/klarna_payments/w /external/me/r /external/me/w

redirect_uri: http://localhost:9292/landing
confirmation_uri: http://localhost:9292/confirmation
organisation_id: 
product_code: 
campaign_code:
period_type: 
period_id: 

auth_uri: https://payway-api.stage.adeprimo.se/oauth2/auth
access_token_uri: https://payway-api.stage.adeprimo.se/api/authorization/access_token

active_product_codes_uri: https://payway-api.stage.adeprimo.se/external/api/v1/me/active_products
me_payment_session_uri: https://payway-api.stage.adeprimo.se/external/api/v2/klarna_payments/me/create_session
payment_session_without_account_uri: https://payway-api.stage.adeprimo.se/external/api/v2/klarna_payments/create_session
me_place_order_uri: https://payway-api.stage.adeprimo.se/external/api/v2/klarna_payments/me/place_order
place_order_without_account_uri: https://payway-api.stage.adeprimo.se/external/api/v2/klarna_payments/place_order
me_place_trial_order_uri: https://payway-api.stage.adeprimo.se/external/api/v2/klarna_payments/me/place_trial_order
place_trial_order_without_account_uri: https://payway-api.stage.adeprimo.se/external/api/v2/klarna_payments/place_trial_order
```

## Starting the application

In the directory where you have this application, first install dependencies:
```
bundle install
```

Then start the application:
```
bundle exec rackup
```