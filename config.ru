require './app'

use Rack::Static, :urls => ["/public"]

map '/' do
  run App
end
