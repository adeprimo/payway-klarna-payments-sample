require 'rest-client'
require 'json'

class OAuthError < StandardError
end

class PaywayClient
  class << self
    def get(url, headers)
      p "Start request"
      p "GET #{url}"
      p "Headers: #{headers}"
      p "End request"
      response = RestClient.get(url, headers)
      result = JSON.parse(response.body, :symbolize_names => true)
      p "Start response"
      p "Headers #{response.headers}"
      p "Body: #{result}"
      p "End response"
      result
    rescue RestClient::ExceptionWithResponse => e
      handle_error(e)
    end

    def post(url, payload, headers)
      p "Start request"
      p "POST #{url}"
      p "Headers: #{headers}"
      p "Payload: #{payload}"
      p "End request"
      response = RestClient.post(url, payload, headers)
      result = JSON.parse(response.body, :symbolize_names => true)
      p "Start response"
      p "Headers #{response.headers}"
      p "Body: #{result}"
      p "End response"
      result
    rescue RestClient::ExceptionWithResponse => e
      handle_error(e)
    end

    private

    def handle_error(e)
      p e.response.code
      p e.response.body
      p e.response.headers
      if e.response.code == 401 and (e.response.headers[:www_authenticate].include?('expired_token') or e.response.headers[:www_authenticate].include?('invalid_token'))
        raise OAuthError.new('token invalid or expired')
      else
        raise e
      end
    end
  end
end