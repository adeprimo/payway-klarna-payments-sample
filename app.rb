require 'sinatra/base'
require 'sinatra/config_file'
require_relative 'payway_client'

class App < Sinatra::Base
  register Sinatra::ConfigFile

  enable :sessions
  config_file 'settings/config.yml'

  # Render the first page where the user is presented with the article teaser.
  # We strongly recommend separating the article teaser and the purchase flow into separate pages.
  get '/' do
    if session[:access_token]
      has_article_access = has_article_access?
      erb :index, :locals => {:logged_in => logged_in?, :product_code => settings.product_code, :campaign_code => settings.campaign_code, :has_article_access => has_article_access, :layout => :layout }
    else
      redirect '/login'
    end
  end

  get '/index_without_account' do
    if session[:access_token]
      erb :index_without_account, :locals => {:logged_in => false, :product_code => settings.product_code,  :campaign_code => settings.campaign_code, :has_article_access => has_article_access? }, :layout => :layout
    else
      erb :index_without_account, :locals => {:logged_in => false, :product_code => settings.product_code,  :campaign_code => settings.campaign_code, :has_article_access => false }, :layout => :layout
    end
  end

  get '/login' do
    erb :login, :locals => {:logged_in => logged_in?}
  end

  post '/login' do
    user = params[:user]
    pwd = params[:pwd]

    create_access_token(user, pwd)

    redirect '/'
  end

  get '/logout' do
    session[:access_token] = nil
    session[:refresh_token] = nil
    redirect '/'
  end

  # Here we establish a payment session towards payway and klarna.
  # This is also where the purchase flow will be presented to the user.
  get '/buy' do
    payload = {
        :code => settings.product_code,
        :period_type => settings.period_type,
        :confirmation_url => settings.confirmation_uri
    }
    payload[:period_id] = settings.period_id if settings.period_id
    begin
      response = create_payment_session(payload)
      session[:klarna_payments_session_id] = response.klarna_payments_session_id
      erb :buy, :locals => {:logged_in => logged_in?, :klarna_client_token => response.klarna_client_token, :payment_method_categories => response[:payment_method_categories]}, :layout => :layout
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => logged_in?}
    end
  end

  get '/buy_without_account' do
    create_access_token_without_account
    payload = {
      :code => settings.product_code,
      :period_type => settings.period_type,
      :confirmation_url => settings.confirmation_uri
    }
    payload[:period_id] = settings.period_id if settings.period_id
    begin
      response = create_payment_session_without_account(payload)
      session[:klarna_payments_session_id] = response.klarna_payments_session_id
      customer_info = {
        :contact_email => params[:contact_email],
        :first_name => params[:first_name],
        :last_name => params[:last_name]
      }
      erb :buy_without_account, :locals => {:klarna_client_token => response.klarna_client_token, :payment_method_categories => response[:payment_method_categories], :logged_in => false, :customer_info => customer_info}, :layout => :layout
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => false}
    end
  end

  get '/buy_trial_without_account' do
    create_access_token_without_account
    payload = {
      :code => settings.campaign_code,
      :period_type => settings.period_type,
      :confirmation_url => settings.confirmation_uri
    }
    payload[:period_id] = settings.period_id if settings.period_id
    begin
      response = create_payment_session_without_account(payload)
      session[:klarna_payments_session_id] = response.klarna_payments_session_id
      customer_info = {
        :contact_email => params[:contact_email],
        :first_name => params[:first_name],
        :last_name => params[:last_name]
      }
      erb :buy_trial_without_account, :locals => {:klarna_client_token => response.klarna_client_token, :payment_method_categories => response[:payment_method_categories], :logged_in => false, :customer_info => customer_info}, :layout => :layout
    rescue StandardError => ex
      p ex.message
      erb :error, :layout => :layout, :locals => {:logged_in => false}
    end
  end

  post '/buy_without_account' do
    payload = {
      :klarna_authorization_token => params[:klarna_authorization_token],
      :klarna_payments_session_id => session[:klarna_payments_session_id],
      :browser_ip => request.ip,
      :browser_language => request.env['HTTP_ACCEPT_LANGUAGE'].split(',')[0],
      :browser_user_agent => request.user_agent,
      :raise_on_ssn_already_taken_error => false,
      :customer_info => {
        :email => params[:contact_email],
        :first_name => params[:first_name],
        :last_name => params[:last_name]
      }
    }
    payload.merge!(:paywall_id => settings.paywall_id) if settings.respond_to?(:paywall_id)

    begin
      response = place_order_without_account(payload)
      # Here we must send the user's browser to klarna, klarna will in turn take care of sending the user to the confirmation url given in the request above.
      redirect response.redirect_url
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => logged_in?}
    end
  end

  post '/buy_trial_without_account' do
    payload = {
      :klarna_authorization_token => params[:klarna_authorization_token],
      :klarna_payments_session_id => session[:klarna_payments_session_id],
      :browser_ip => request.ip,
      :browser_language => request.env['HTTP_ACCEPT_LANGUAGE'].split(',')[0],
      :browser_user_agent => request.user_agent,
      :raise_on_ssn_already_taken_error => false,
      :customer_info => {
        :email => params[:contact_email],
        :first_name => params[:first_name],
        :last_name => params[:last_name]
      }
    }
    payload.merge!(:paywall_id => settings.paywall_id) if settings.respond_to?(:paywall_id)

    begin
      response = place_trial_order_without_account(payload)
      # Here we must send the user's browser to klarna, klarna will in turn take care of sending the user to the confirmation url given in the request above.
      redirect response.redirect_url
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => logged_in?}
    end
  end

  # The user approves the purchase. We use the authorization token you receive from klarna to complete the purchase in payway and klarna
  post '/buy' do
    payload = {
        :klarna_authorization_token => params[:klarna_authorization_token],
        :klarna_payments_session_id => session[:klarna_payments_session_id],
        :browser_ip => request.ip,
        :browser_language => request.env['HTTP_ACCEPT_LANGUAGE'].split(',')[0],
        :browser_user_agent => request.user_agent,
        :raise_on_ssn_already_taken_error => false
    }
    payload.merge!(:paywall_id => settings.paywall_id) if settings.respond_to?(:paywall_id)

    begin
      response = place_order(payload)
      # Here we must send the user's browser to klarna, klarna will in turn take care of sending the user to the confirmation url given in the request above.
      redirect response.redirect_url
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => logged_in?}
    end
  end

  get '/buy_trial' do
    payload = {
        :code => settings.campaign_code,
        :period_type => settings.period_type,
        :confirmation_url => settings.confirmation_uri,
        :period_id => settings.period_id
    }
    begin
      response = create_payment_session(payload)
      session[:klarna_payments_session_id] = response.klarna_payments_session_id
      erb :buy_trial, :locals => {:logged_in => logged_in?, :klarna_client_token => response.klarna_client_token }, :layout => :layout
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => logged_in?}
    end
  end

  # The user approves the purchase. We use the authorization token you receive from klarna to complete the purchase in payway and klarna
  post '/buy_trial' do
    payload = {
        :klarna_authorization_token => params[:klarna_authorization_token],
        :klarna_payments_session_id => session[:klarna_payments_session_id],
        :browser_ip => request.ip,
        :browser_language => request.env['HTTP_ACCEPT_LANGUAGE'].split(',')[0],
        :browser_user_agent => request.user_agent
    }
    begin
      response = place_trial_order(payload)
      # Here we must send the user's browser to klarna, klarna will in turn take care of sending the user to the confirmation url given in the request above.
      redirect response.redirect_url
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => logged_in?}
    end
  end

  get '/confirmation' do
    begin
      erb :confirmation, :layout => :layout, :locals => {:logged_in => logged_in?}
    rescue StandardError
      erb :error, :layout => :layout, :locals => {:logged_in => logged_in?}
    end
  end

  private

  def create_access_token_without_account
    payload = {
      'grant_type': 'none',
      'client_id': settings.client_id,
      'client_secret': settings.client_secret,
      'scope': settings.scopes
    }
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }
    response = PaywayClient.post(settings.access_token_uri, payload, headers)
    session[:access_token] = response[:access_token]
    session[:refresh_token] = response[:refresh_token] if response[:refresh_token]
  end

  ####################################################
  # Create access_token with identity using password grant. Not a required way of implementing.
  # Read more on acquiring access token with identity.
  # https://docs.worldoftulo.com/payway/integration/payway_api/api_access/#access-token
  # ####################################
  def create_access_token(user, pwd)
    payload = {
      'grant_type': 'password',
      'client_id': settings.client_id,
      'client_secret': settings.client_secret,
      'scope': settings.scopes,
      'username': "#{settings.organisation_id}^#{user}",
      'password': pwd
    }
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }
    response = PaywayClient.post(settings.access_token_uri, payload, headers)
    session[:access_token] = response[:access_token]
    session[:refresh_token] = response[:refresh_token] if response[:refresh_token]
  end

  ####################################################
  # Acquire new access_token from refresh token
  # Read more on acquiring access token with refresh token
  # https://docs.worldoftulo.com/payway/integration/payway_api/api_access/#handling-access-token-expiry-with-refresh-tokens
  # ####################################
  def create_access_token_from_refresh_token
    headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }
    payload = {
      'grant_type': 'refresh_token',
      'refresh_token': session[:refresh_token],
      'client_id': settings.client_id,
      'client_secret': settings.client_secret,
      'redirect_uri': 'http://localhost:9292'
    }
    response = PaywayClient.post(settings.access_token_uri, payload, headers)
    session[:access_token] = response[:access_token]
    session[:refresh_token] = response[:refresh_token] if response[:refresh_token]
  end

  def create_payment_session(payload)
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    }
    response = PaywayClient.post(payment_session_url, payload.to_json, headers)
    OpenStruct.new({:klarna_payments_session_id => response[:item][:klarna_payments_session_id], :klarna_client_token => response[:item][:klarna_client_token], :payment_method_categories => response[:item][:payment_method_categories]})
  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def create_payment_session_without_account(payload)
    headers = {
      'Authorization' => "Bearer #{session[:access_token]}",
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    }
    response = PaywayClient.post(payment_session_without_account_url, payload.to_json, headers)
    OpenStruct.new({:klarna_payments_session_id => response[:item][:klarna_payments_session_id], :klarna_client_token => response[:item][:klarna_client_token], :payment_method_categories => response[:item][:payment_method_categories]})
  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def place_order(payload)
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    }
    response = PaywayClient.post(place_order_url, payload.to_json, headers)
    OpenStruct.new({:redirect_url => response[:item][:redirect_url]})

  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def place_order_without_account(payload)
    headers = {
      'Authorization' => "Bearer #{session[:access_token]}",
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    }
    response = PaywayClient.post(place_order_without_account_url, payload.to_json, headers)
    OpenStruct.new({:redirect_url => response[:item][:redirect_url]})

  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def place_trial_order_without_account(payload)
    headers = {
      'Authorization' => "Bearer #{session[:access_token]}",
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    }
    response = PaywayClient.post(place_trial_order_without_account_url, payload.to_json, headers)
    OpenStruct.new({:redirect_url => response[:item][:redirect_url]})

  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def place_trial_order(payload)
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Content-Type' => 'application/json',
        'Accept' => 'application/json'
    }
    response = PaywayClient.post(place_trial_order_url, payload.to_json, headers)
    OpenStruct.new({:redirect_url => response[:item][:redirect_url]})

  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def logged_in?
    session[:access_token] != nil
  end

  def has_article_access?
    headers = {
        'Authorization' => "Bearer #{session[:access_token]}",
        'Accept' => 'application/json'
    }
    response = PaywayClient.get(active_product_codes_url, headers)
    response[:item][:active_products].include?(settings.product_code)
  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def get_access_token_data(code)
    headers = {
      'Authorization' => "Bearer #{session[:access_token]}",
      'Content-Type' => 'application/json',
      'Accept' => 'application/json'
    }
    params = {
        :grant_type => "authorization_code",
        :code => code,
        :client_id => settings.client_id,
        :client_secret => settings.client_secret,
        :redirect_uri => settings.redirect_uri,
        :scope => settings.scopes
    }
    PaywayClient.post(access_token_url, params, headers)
  rescue RestClient::ExceptionWithResponse => e
    parse_error(e.response.code, JSON.parse(e.response.body, :symbolize_names => true))
    raise e
  end

  def access_token_url
    "#{settings.access_token_uri}"
  end

  def payment_session_url
    "#{settings.me_payment_session_uri}"
  end

  def payment_session_without_account_url
    "#{settings.payment_session_without_account_uri}"
  end

  def place_order_url
    "#{settings.me_place_order_uri}"
  end

  def place_order_without_account_url
    "#{settings.place_order_without_account_uri}"
  end

  def place_trial_order_without_account_url
    "#{settings.place_trial_order_without_account_uri}"
  end

  def place_trial_order_url
    "#{settings.me_place_trial_order_uri}"
  end

  def active_product_codes_url
    "#{settings.active_product_codes_uri}"
  end

  private

  def parse_error(code, error)
    puts "#{code} - #{error}"
    "#{code} - #{error}"
  end
end